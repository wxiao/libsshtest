#include <QDebug>

#include "sftpfilesystem.h"

QSshSession *initSession(QString host, QString user, QString password)
{
    QSshSession *session = QSshSession::connectToHost(host, user);
    
    if (!session->isConnected()) {
        delete session;
        return nullptr;
    }

    session->authenticateByPassword(password);
    if (!session->isAuthorized()) {
        delete session;
        return nullptr;
    }
    
    session.sftp()->connect();
    if (!session->sftp()->isConnected()) {
        delete session;
        return nullptr;
    }
    
    return session;
}

SftpFileItem::SftpFileItem(QSftpFile sftpFile, QUrl parentUrl, QString user) {
    QString name = sftpFile.filename();

    if (name.endsWith(QStringLiteral("/"))) {
        // Remove the last '/' in file name
        name.chop(1);
    }

    //NSURL *nsParentURL = parentUrl.toNSURL();   // This NSURL is autoreleased
    //NSURL *url = [nsParentURL URLByAppendingPathComponent: name.toNSString() isDirectory: sftpFile.isDirectory];

    int flags = 0;

    // sftpFile.isWritable(by: user)
    m_flags |= 1 << 2;

    // sftpFile.isReadable(by: user)
    m_flags |= 1 << 3;
    
    if (sftpFile.isDirectory()) {
        // flags.insert(.isDirectory);
        m_flags |= 1 << 0;
    }
    
    if (name.startsWith(QStringLiteral("."))) {
        // Hidden file
        m_flags |= 1 << 1;
    }

    if (m_flags & 1 << 0) {
        // Is a directory
        m_icon = QIcon::fromTheme(QStringLiteral("folder"));
    } else {
        // Is a normal file
        // To-Do
        m_icon = QIcon::fromTheme(QStringLiteral("file"));
    }

    // To-Do
    m_url = parentUrl;

    m_name = name;
}

SftpFileSystem::SftpFileSystem(const QString name, const QString host, const int port, const QString user, const QString password, const QString path)
{
    m_name = name;

    m_browseSession = initSession(host + QStringLiteral(":") + QString::number(port), user, password);
    m_fileOperationsSession = initSession(host + QStringLiteral(":") + QString::number(port), user, password);
    
    QString directoryPath = path;
    if (!directoryPath .endsWith(QStringLiteral("/"))) {
        directoryPath = directoryPath + QStringLiteral("/");
    }

    NSString *url = [NSString stringWithFormat:@"sftp:%@@%@:%d%@", 
        user.toNSString(), host.toNSString(), port, directoryPath.toNSString()];

    NSURL *rootUrl = [NSURL URLWithString:url];

    NSLog(@"absoluteURL = %@", [rootUrl absoluteURL]);

    m_rootUrl = QUrl::fromNSURL(rootUrl);

    [rootUrl release];
    [url release];
}

SftpFileSystem::~SftpFileSystem()
{
    if (m_browseSession != nil) {
        m_browseSession->disconnect();
        delete m_browseSession;
    }

    if (m_fileOperationsSession != nil) {
        m_fileOperationsSession->disconnect();
        delete m_fileOperationsSession;
    }
}

QList<FileItem> SftpFileSystem::loadFiles(const QUrl url)
{
    if (!m_browseSession) {
        return {};
    }

    QVector<QSftpFile> files = m_browseSession->sftp()->contentsOfDirectoryAtPath(url.path());

    QList<FileItem> fileItems;
    for (QSftpFile file : files) {
        SftpFileItem item(file, url, m_browseSession->username());
        fileItems << item;
    }

    return fileItems;
}

void SftpFileSystem::deleteFile(const QUrl url)
{

}

void SftpFileSystem::copyFile(const QUrl srcUrl, const QUrl destUrl)
{

}

void SftpFileSystem::moveFile(const QUrl srcUrl, const QUrl destUrl)
{

}

void SftpFileSystem::createFolder(const QUrl url)
{

}

SftpFileListModel::SftpFileListModel(QObject* parent) : FileListModel(parent)
{
    m_fileSystem = new SftpFileSystem(QStringLiteral("Cici"),
        QStringLiteral("127.0.0.1"),
        22,
        QStringLiteral("cici"),
        QStringLiteral("***"),
        QStringLiteral("/tmp")
    );

    load();
}

SftpFileListModel::~SftpFileListModel()
{
    if (m_fileSystem != nullptr) {
        delete m_fileSystem;
    }
}

void SftpFileListModel::load()
{
    clear();

    QList<FileItem> fileItems = m_fileSystem->loadFiles(QUrl(m_path));

    qWarning() << fileItems.size() << m_path;

    for (FileItem fileItem : fileItems) {
        QStandardItem *item = new QStandardItem();
        item->setData(fileItem.name(), FileListModel::NameRole);
        appendRow(item);
    }
}
