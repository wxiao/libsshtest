#include "nmsshsftpfilesystem.h"

#include <QDebug>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QQmlContext>
#include <KAboutData>
#include <KLocalizedString>
#include <KLocalizedContext>
#include <KDBusService>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    KAboutData aboutData(QStringLiteral("kdeconnect.sftpbrowser"),
                         i18n("SFTP browser"),
                         QStringLiteral(""),
                         i18n("KDE Connect SFTP browser"),
                         KAboutLicense::GPL_V3,
                         i18n("(C) 2019, KDE Connect Team"));
    KAboutData::setApplicationData(aboutData);

    qmlRegisterType<NMSSHSftpFileListModel>("org.kde.kdeconnect.sftp", 1, 0, "NMSSHSftpFileListModel");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("main.qml")));

    return app.exec();
}
