#include "sftpfilesystem.h"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QQmlContext>
#include <KAboutData>
#include <KLocalizedString>
#include <KLocalizedContext>
#include <KDBusService>

int main() {
    SftpFileSystem *sftp = new SftpFileSystem(QStringLiteral("Cici"),
        QStringLiteral("127.0.0.1"),
        22,
        QStringLiteral("cici"),
        QStringLiteral("093052Hx"),
        QStringLiteral("/tmp")
    );

    sftp->loadFiles(QUrl(QStringLiteral("/tmp")));

    delete sftp;
    return 0;
}
