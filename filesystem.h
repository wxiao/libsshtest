
#ifndef __FILE_SYSTEM__
#define __FILE_SYSTEM__

#include <QUrl>
#include <QString>
#include <QIcon>
#include <QList>
#include <QStandardItem>
#include <QStandardItemModel>

class FileItem
{
public:
    FileItem();
    FileItem(QUrl url, QString name, QIcon icon, int flags);

    int flags();
    bool isDirectory();
    bool isHidden();
    bool isReadable();
    bool isWritable();
    bool isBusy();
    bool isDeleted();
    bool canRead();
    bool canModify();

    QString name();
protected:
    int m_flags;
    QUrl m_url;
    QString m_name;
    QIcon m_icon;
};

class Place {
public:
    Place(QString name, QUrl url);
private:
    QString m_name;
    QUrl m_url;
};

class FileSystem
{
public:
    virtual ~FileSystem();

    QString name();
    QUrl rootUrl();
    QList<Place> places();

    // Read file list for provided URL. URL must reside under rootUrl.
    virtual QList<FileItem> loadFiles(const QUrl url) = 0;

    // Delete file at provided URL. URL must reside under rootUrl
    virtual void deleteFile(const QUrl url) = 0;

    // Copy file from one place to another. At least one URL must reside under rootUrl. destUrl must indicate the
    // final copied file name, not the directory containing it.
    virtual void copyFile(const QUrl srcUrl, const QUrl destUrl) = 0;

    // Move file from one place to another. Both URLs must reside under rootUrl
    virtual void moveFile(const QUrl srcUrl, const QUrl destUrl) = 0;

    // Create folder at specified url, which must be under file system root
    virtual void createFolder(const QUrl url) = 0;
protected:
    QString m_name;
    QUrl m_rootUrl;
    QList<Place> m_places;
};

class FileListModel : public QStandardItemModel
{
public:
    FileListModel(QObject* parent = nullptr);
    virtual ~FileListModel();

    Q_SCRIPTABLE QString currentUrl() const;
    //void setCurrentUrl(QString url) {};

    Q_SCRIPTABLE virtual void load() = 0;

    enum Roles {
        /* Roles which apply while working as a single message */
        NameRole = Qt::UserRole,
    };
    Q_ENUM(Roles)
protected:
    FileSystem *m_fileSystem;
    QString m_path;
};

#endif