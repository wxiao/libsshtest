#ifndef __SFTP_HELPER__
#define __SFTP_HELPER__

#include <libssh2.h>
#include <libssh2_sftp.h>

#include <QHostAddress>
#include <QVector>
#include <QDate>

namespace QSftpHelper {

class QSshSession;

class QSftpFile
{
public:
    static QSftpFile fileWithName(QString filename);
    QSftpFile(QString filename);
    QSftpFile(QString filename, LIBSSH2_SFTP_ATTRIBUTES fileAttributes);

    void populateValuesFromSFTPAttributes(LIBSSH2_SFTP_ATTRIBUTES fileAttributes);

    QString filename();
    bool isDirectory();
    QDateTime modificationDate();
    QDateTime lastAccess();
    unsigned long long fileSize();
    unsigned long ownerUserID();
    unsigned long ownerGroupID();
    QString permissions();
    unsigned long flags();
protected:
    QString m_filename;
    bool m_isDirectory;
    QDateTime m_modificationDate;
    QDateTime m_lastAccess;
    unsigned long long m_fileSize;
    unsigned long m_ownerUserID;
    unsigned long m_ownerGroupID;
    QString m_permissions;
    unsigned long m_flags;
};

class QSftp
{
public:
    static QSftp *connectWithSession(QSshSession *session);

    QSftp(QSshSession *session);

    bool connect();
    void disconnect();

    LIBSSH2_SFTP_HANDLE *openDirectoryAtPath(const QString path);
    LIBSSH2_SFTP_HANDLE *openFileAtPath(QString path, unsigned long flags, long mode);
    
    bool directoryExistsAtPath(const QString path);
    bool fileExistsAtPath(const QString path);

    bool moveItemAtPath(const QString sourcePath, const QString destPath);
    bool createDirectoryAtPath(const QString path);
    bool removeDirectoryAtPath(const QString path);

    bool createSymbolicLinkAtPath(const QString linkPath, const QString destPath);
    removeFileAtPath:(NSString *)path

    QVector<QSftpFile> contentsOfDirectoryAtPath(QString path);
    QSftpFile infoForFileAtPath(QString path);
    QSftpFile infoForSFTPHandle(LIBSSH2_SFTP_HANDLE *handle, const QString filename);

    bool isConnected();
private:
    QSshSession *m_session;
    LIBSSH2_SFTP *m_sftpSession;
    bool m_isConnected;
    int m_bufferSize;
};

class QSshChannel
{
private:
    QSshSession *m_session;
    int m_bufferSize;
    int m_type;
};

class QSshHostConfig
{

};

class QSshSession
{
public:
    static QSshSession *connectToHost(const QString host, const int port, const QString username);
    static QSshSession *connectToHost(const QString host, const QString username);

    static QUrl getURLFromHost(const QString host);

    QSshSession(const QString host, const int port, const QString username);
    QSshSession(const QString host, const QString username);
    ~QSshSession();

    bool connect();
    bool connectWithTimeout(int timeout);
    void disconnect();
    bool connectToAgent();

    QString remoteBanner();
    QVector<QString> supportedAuthenticationMethods();
    QString fingerprint(int hashType);

    void setTimeout(long timeout);
    long timeout();
    QString lastError();

    bool isAuthorized();

    bool authenticate();
    bool authenticate(QString password);
    bool authenticate(QString publicKey, QString privateKey, QString password);

    QVector<QHostAddress> hostIPAddresses();

    QString userKnownHostsFileName();
    QString systemKnownHostsFileName();

    LIBSSH2_SESSION *rawSession();

    QSshChannel *channel();
    QSftp *sftp();
Q_SIGNALS:
    /**
     * Emitted if connection is disconnected
     */
    void disconnected(LIBSSH2_SESSION *session, int reason,
        const char *message, int message_len, const char *language,
        int language_len, void **abstract
    );

    /**
     * Emitted if connection is established
     */
    void connected();

protected:
    QSshChannel *m_channel;
    QSftp *m_sftp;

    LIBSSH2_AGENT *m_agent
    LIBSSH2_SESSION *m_session;

    QString *m_host;
    QString *m_username;
    NMSSHHostConfig *m_hostConfig;
    int m_port;
    // NSString *(^kbAuthenticationBlock)(NSString *);

    LIBSSH2_SESSION *m_sessionToFree;
};

}

#endif //__SFTP_HELPER__