#include "sftphelper.h"

char filetypeletter(unsigned long mode)
{
    char c;
    
    if (S_ISREG(mode)) {
        c = '-';
    } else if (S_ISDIR(mode)) {
        c = 'd';
    } else if (S_ISBLK(mode)) {
        c = 'b';
    } else if (S_ISCHR(mode)) {
        c = 'c';
    }
#ifdef S_ISFIFO
    else if (S_ISFIFO(mode)) {
        c = 'p';
    }
#endif
#ifdef S_ISLNK
    else if (S_ISLNK(mode)) {
        c = 'l';
    }
#endif
#ifdef S_ISSOCK
    else if (S_ISSOCK(mode)) {
        c = 's';
    }
#endif
#ifdef S_ISDOOR
    // Solaris 2.6, etc.
    else if (S_ISDOOR(mode)) {
        c = 'D';
    }
#endif
    else {
        // Unknown type -- possibly a regular file?
        c = '?';
    }

    return c;
}

QString convertPermissionToSymbolicNotation(unsigned long mode)
{
    static char *rwx[] = {"---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx"};
    char bits[11];
    
    bits[0] = filetypeletter(mode);
    strcpy(&bits[1], rwx[(mode >> 6)& 7]);
    strcpy(&bits[4], rwx[(mode >> 3)& 7]);
    strcpy(&bits[7], rwx[(mode & 7)]);

    if (mode & S_ISUID) {
        bits[3] = (mode & 0100) ? 's' : 'S';
    }
    if (mode & S_ISGID) {
        bits[6] = (mode & 0010) ? 's' : 'l';
    }
    if (mode & S_ISVTX) {
        bits[9] = (mode & 0100) ? 't' : 'T';
    }

    bits[10] = '\0';

    return QString(bits);
}

static QSftpFile QSftpFile::fileWithName(QString filename)
{
    return QSftpFile(filename);
}

QSftpFile::QSftpFile(QString filename)
    : m_filename(filename)
      m_isDirectory(false),
      m_fileSize(0),
      m_ownerUserID(0),
      m_ownerGroupID(0),
      m_flags(0) {}

QSftpFile::QSftpFile(QString filename, LIBSSH2_SFTP_ATTRIBUTES fileAttributes) : m_filename(filename)
{
    populateValuesFromSFTPAttributes(fileAttributes);
}

QString QSftpFile::filename()
{
    return m_filename;
}

bool QSftpFile::isDirectory()
{
    return m_isDirectory;
}

QDate QSftpFile::modificationDate()
{
    return m_modificationDate;
}

QDate QSftpFile::lastAccess()
{
    return m_lastAccess;
}

unsigned long long QSftpFile::fileSize()
{
    return m_fileSize;
}

unsigned long QSftpFile::ownerUserID()
{
    return m_ownerUserID;
}

unsigned long QSftpFile::ownerGroupID()
{
    return m_ownerGroupID;
}

QString QSftpFile::permissions()
{
    return m_permissions;
}

unsigned long QSftpFile::flags()
{
    return m_flags;
}

void QSftpFile::populateValuesFromSFTPAttributes(LIBSSH2_SFTP_ATTRIBUTES fileAttributes)
{
    m_modificationDate= QDateTime::fromSecsSinceEpoch(fileAttributes.mtime);
    m_lastAccess = QDateTime::fromSecsSinceEpoch(fileAttributes.atime);
    m_fileSize = fileAttributes.filesize;
    m_ownerUserID = fileAttributes.uid;
    m_ownerGroupID = fileAttributes.gid;
    m_permissions = convertPermissionToSymbolicNotation(fileAttributes.permissions);
    m_isDirectory = LIBSSH2_SFTP_S_ISDIR(fileAttributes.permissions);
    m_flags = fileAttributes.flags;
}


/* QSftp */
static QSftp *QSftp::connectWithSession(QSshSession *session)
{
    QSftp *sftp = new QSftp(session);
    sftp->connect();
    return sftp;
}

QSftp::QSftp(QSshSession *session)
    : m_session(session) {}

bool QSftp::connect()
{
    // Set blocking mode
    libssh2_session_set_blocking(m_session->rawSession(), 1);

    m_sftpSession = libssh2_sftp_init(m_session->rawSession());

    if (!m_sftpSession) {
        return false;
    }

    m_isConnected = true;
    m_bufferSize = 0x4000;

    return m_isConnected;
}

bool QSftp::disconnect()
{
    libssh2_sftp_shutdown(m_sftpSession);
    m_isConnected = false;
}

bool QSftp::moveItemAtPath(const QString sourcePath, const QString destPath)
{
    return libssh2_sftp_rename(m_sftpSession, sourcePath.toStdString().c_str(), destPath.toStdString().c_str()) == 0;
}

LIBSSH2_SFTP_HANDLE *QSftp::openDirectoryAtPath(const QString path)
{
    LIBSSH2_SFTP_HANDLE *handle = libssh2_sftp_opendir(m_sftpSession, path.toStdString().c_str());

    if (!handle) {
        /*
        NSError *error = [self.session lastError];
        NMSSHLogError(@"Could not open directory at path %@ (Error %li: %@)", path, (long)error.code, error.localizedDescription);

        if ([error code] == LIBSSH2_ERROR_SFTP_PROTOCOL) {
            NMSSHLogError(@"SFTP error %lu", libssh2_sftp_last_error(self.sftpSession));
        }
        */
    }

    return handle;
}

bool QSftp::directoryExistsAtPath(const QString path)
{
    LIBSSH2_SFTP_HANDLE *handle = openFileAtPath(path, LIBSSH2_FXF_READ, 0);

    if (!handle) {
        return false;
    }

    LIBSSH2_SFTP_ATTRIBUTES fileAttributes;
    int rc = libssh2_sftp_fstat(handle, &fileAttributes);
    libssh2_sftp_close(handle);

    return rc == 0 && LIBSSH2_SFTP_S_ISDIR(fileAttributes.permissions);
}

bool fileExistsAtPath(const QString path)
{
    LIBSSH2_SFTP_HANDLE *handle = openFileAtPath(path, LIBSSH2_FXF_READ, 0);

    if (!handle) {
        return false;
    }

    LIBSSH2_SFTP_ATTRIBUTES fileAttributes;
    int rc = libssh2_sftp_fstat(handle, &fileAttributes);
    libssh2_sftp_close(handle);

    return rc == 0 && !LIBSSH2_SFTP_S_ISDIR(fileAttributes.permissions);
}

bool QSftp::createDirectoryAtPath(const QString path)
{
    int rc = libssh2_sftp_mkdir(m_sftpSession, path.toStdString().c_str(), LIBSSH2_SFTP_S_IRWXU |
        LIBSSH2_SFTP_S_IRGRP | LIBSSH2_SFTP_S_IXGRP| LIBSSH2_SFTP_S_IROTH | LIBSSH2_SFTP_S_IXOTH);

    return rc == 0;
}

bool QSftp::removeDirectoryAtPath(const QString path)
{
    return libssh2_sftp_rmdir(m_sftpSession, path.toStdString().c_str()) == 0;
}

QVector<QSftpFile> QSftp::contentsOfDirectoryAtPath(QString path)
{
    LIBSSH2_SFTP_HANDLE *handle = openDirectoryAtPath(path);

    if (!handle) {
        return {};
    }

    QVector contents;

    int rc;
    do {
        char buffer[512];
        LIBSSH2_SFTP_ATTRIBUTES fileAttributes;

        rc = libssh2_sftp_readdir(handle, buffer, sizeof(buffer), &fileAttributes);

        if (rc > 0) {
            QString filename = QString::fromLocal8Bit(buffer, rc);
            if (filename != QStringLiteral(".") && filename != QStringLiteral("..")) {
                // Append a "/" at the end of all directories
                if (LIBSSH2_SFTP_S_ISDIR(fileAttributes.permissions)) {
                    filename += QStringLiteral("/");
                }

                QSftpFile file = QSftpFile::fileWithName(filename);
                file.populateValuesFromSFTPAttributes(fileAttributes);
                contents << file;
            }
        }
    } while (rc > 0);

    if (rc < 0) {
    }

    rc = libssh2_sftp_closedir(handle);

    if (rc < 0) {
    }

    return contents;
}


// QSshSession
LIBSSH2_SESSION *QSshSession::rawSession()
{
    return m_session;
}

QSshChannel *QSshSession::channel()
{
    // TO DO
    return nullptr;
}

QSftp *QSshSession::sftp()
{
    // On-demand
    if (!m_sftp) {
        m_sftp = QSftp::initWithSession(this);
    }

    return m_sftp;
}
