#include "filesystem.h"
#include <QDebug>

FileItem::FileItem() : m_flags(0) {}

FileItem::FileItem(QUrl url, QString name, QIcon icon, int flags)
    : m_url(url), m_name(name), m_icon(icon), m_flags(flags) {}

int FileItem::flags()
{
    return m_flags;
}

bool FileItem::isDirectory()
{
    return (m_flags & 1 << 0) != 0;
}

bool FileItem::isHidden()
{
    return (m_flags & (1 << 1)) != 0;
}

bool FileItem::isReadable()
{
    return (m_flags & 1 << 2) != 0;
}

bool FileItem::isWritable()
{
    return (m_flags & 1 << 3) != 0;
}

bool FileItem::isBusy()
{
    return (m_flags & 1 << 4) != 0;
}

bool FileItem::isDeleted()
{
    return (m_flags & 1 << 5) != 0;
}

bool FileItem::canRead()
{
    return isReadable() && !isBusy() && !isDeleted();
}

bool FileItem::canModify()
{
    return isWritable() && !isBusy() && !isDeleted();
}

QString FileItem::name()
{
    return m_name;
}

QUrl FileSystem::rootUrl()
{
    return m_rootUrl;
}

QList<Place> FileSystem::places()
{
    return m_places;
}

FileSystem::~FileSystem() {}

FileListModel::FileListModel(QObject* parent) 
    : QStandardItemModel(parent),
      m_fileSystem(nullptr),
      m_path(QStringLiteral("/"))
{
    auto roles = roleNames();
    roles.insert(FileListModel::NameRole, "name");
    setItemRoleNames(roles);
}

FileListModel::~FileListModel()
{
}

QString FileListModel::currentUrl() const
{
    if (!m_fileSystem) {
        return QStringLiteral("");
    }

    return m_fileSystem->rootUrl().toString();
}
