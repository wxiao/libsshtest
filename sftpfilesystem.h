#ifndef __SFTP_FILE_SYSTEM__
#define __SFTP_FILE_SYSTEM__

#include "filesystem.h"
#include "sftphelper.h"

class SftpFileItem : public FileItem
{
public:
    SftpFileItem(QSftpFile sftpFile, QUrl parentUrl, QString user);
};

class SftpFileSystem : public FileSystem
{
public:
    SftpFileSystem(const QString name, const QString host, const int port, const QString user, const QString password, const QString path);
    ~SftpFileSystem();

    // Read file list for provided URL. URL must reside under rootUrl.
    QList<FileItem> loadFiles(const QUrl url);

    // Delete file at provided URL. URL must reside under rootUrl
    void deleteFile(const QUrl url);

    // Copy file from one place to another. At least one URL must reside under rootUrl. destUrl must indicate the
    // final copied file name, not the directory containing it.
    void copyFile(const QUrl srcUrl, const QUrl destUrl);

    // Move file from one place to another. Both URLs must reside under rootUrl
    void moveFile(const QUrl srcUrl, const QUrl destUrl);

    // Create folder at specified url, which must be under file system root
    void createFolder(const QUrl url);
private:
    QSshSession *m_browseSession;
    QSshSession *m_fileOperationsSession;
};

class SftpFileListModel : public FileListModel
{
public:
    SftpFileListModel(QObject* parent = nullptr);
    ~SftpFileListModel();

    void load();
};

#endif