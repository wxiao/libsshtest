#import <Foundation/Foundation.h>
#import <Appkit/Appkit.h>

#include <QtMac>
#include <QDebug>

#include "nmsshsftpfilesystem.h"

NMSSHSession *initSession(QString host, QString user, QString password)
{
    NMSSHSession *session = [NMSSHSession connectToHost:host.toNSString() withUsername:user.toNSString()];
    
    if (!session.isConnected) {
        [session release];
        return nil;
    }

    [session authenticateByPassword: password.toNSString()];
    if (!session.isAuthorized) {
        [session release];
        return nil;
    }
    
    [session.sftp connect];
    if (!session.sftp.isConnected) {
        [session release];
        return nil;
    }
    
    return session;
}

NMSSHSftpFileItem::NMSSHSftpFileItem(NMSFTPFile *sftpFile, QUrl parentUrl, QString user) {
    QString name = QString::fromNSString(sftpFile.filename);

    if (name.endsWith(QStringLiteral("/"))) {
        // Remove the last '/' in file name
        name.chop(1);
    }

    NSURL *nsParentURL = parentUrl.toNSURL();   // This NSURL is autoreleased
    NSURL *url = [nsParentURL URLByAppendingPathComponent: name.toNSString() isDirectory: sftpFile.isDirectory];

    int flags = 0;

    // sftpFile.isWritable(by: user)
    m_flags |= 1 << 2;

    // sftpFile.isReadable(by: user)
    m_flags |= 1 << 3;
    
    if (sftpFile.isDirectory) {
        // flags.insert(.isDirectory);
        m_flags |= 1 << 0;
    }
    
    if (name.startsWith(QStringLiteral("."))) {
        // Hidden file
        m_flags |= 1 << 1;
    }

    NSString *fileType;
    if (m_flags & 1 << 0) {
        // Is a directory
        // fileType = QString::fromCFString(kUTTypeDirectory).toNSString();    // This NSString is autoreleased
        m_icon = QIcon::fromTheme(QStringLiteral("folder"));
    } else {
        // Is a normal file
        // To-Do
        // fileType = url.pathExtension;
        m_icon = QIcon::fromTheme(QStringLiteral("file"));
    }

    // NSImage *icon = [[NSWorkspace sharedWorkspace] iconForFileType: fileType];

    m_url = QUrl::fromNSURL(url);

    m_name = name;

    // m_icon = QIcon(fromNSImageToPixmap(icon, 128, 128));

    [url release];
    [sftpFile release];
}

QPixmap NMSSHSftpFileItem::fromNSImageToPixmap(NSImage *nsImage, const int width, const int height)
{
    NSBitmapImageRep * bitmap = [[NSBitmapImageRep alloc]
        initWithBitmapDataPlanes:NULL
        pixelsWide:width
        pixelsHigh:height
        bitsPerSample:8
        samplesPerPixel:4
        hasAlpha:YES
        isPlanar:NO
        colorSpaceName:NSDeviceRGBColorSpace
        bitmapFormat:NSBitmapFormatAlphaFirst
        bytesPerRow:0
        bitsPerPixel:0
    ];
    [NSGraphicsContext saveGraphicsState];
    [NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithBitmapImageRep:bitmap]];

    // assume NSImage nsimage
    [nsImage drawInRect:NSMakeRect(0,0,width,height) fromRect:NSZeroRect operation: NSCompositingOperationSourceOver fraction: 1];
    [NSGraphicsContext restoreGraphicsState];

    CGImageRef image = [bitmap CGImage];

    // There is a segmentation fault
    QPixmap pixmap = QtMac::fromCGImageRef(image);

    [bitmap release];

    return pixmap;
}

NMSSHSftpFileSystem::NMSSHSftpFileSystem(const QString name, const QString host, const int port, const QString user, const QString password, const QString path)
{
    m_name = name;

    m_browseSession = initSession(host + QStringLiteral(":") + QString::number(port), user, password);
    m_fileOperationsSession = initSession(host + QStringLiteral(":") + QString::number(port), user, password);
    
    QString directoryPath = path;
    if (!directoryPath .endsWith(QStringLiteral("/"))) {
        directoryPath = directoryPath + QStringLiteral("/");
    }

    NSString *url = [NSString stringWithFormat:@"sftp:%@@%@:%d%@", 
        user.toNSString(), host.toNSString(), port, directoryPath.toNSString()];

    NSURL *rootUrl = [NSURL URLWithString:url];

    NSLog(@"absoluteURL = %@", [rootUrl absoluteURL]);

    m_rootUrl = QUrl::fromNSURL(rootUrl);

    [rootUrl release];
    [url release];
}

NMSSHSftpFileSystem::~NMSSHSftpFileSystem()
{
    if (m_browseSession != nil) {
        [m_browseSession disconnect];
    }

    if (m_fileOperationsSession != nil) {
        [m_fileOperationsSession disconnect];
    }
}

QList<FileItem> NMSSHSftpFileSystem::loadFiles(const QUrl url)
{
    if (!m_browseSession) {
        NSLog(@"No browse session");
        return QList<FileItem>();
    }

    NSArray<NMSFTPFile*> *files = [m_browseSession.sftp contentsOfDirectoryAtPath:url.path().toNSString()];

    QList<FileItem> fileItems;
    for (NMSFTPFile *file in files) {
        NMSSHSftpFileItem item(file, url, QString::fromNSString(m_browseSession.username));
        fileItems << item;
    }

    [files release];

    return fileItems;
}

void NMSSHSftpFileSystem::deleteFile(const QUrl url)
{

}

void NMSSHSftpFileSystem::copyFile(const QUrl srcUrl, const QUrl destUrl)
{

}

void NMSSHSftpFileSystem::moveFile(const QUrl srcUrl, const QUrl destUrl)
{

}

void NMSSHSftpFileSystem::createFolder(const QUrl url)
{

}

NMSSHSftpFileListModel::NMSSHSftpFileListModel(QObject* parent) : FileListModel(parent)
{
    m_fileSystem = new NMSSHSftpFileSystem(QStringLiteral("Cici"),
        QStringLiteral("127.0.0.1"),
        22,
        QStringLiteral("cici"),
        QStringLiteral("***"),
        QStringLiteral("/tmp")
    );

    load();
}

NMSSHSftpFileListModel::~NMSSHSftpFileListModel()
{
    if (m_fileSystem != nullptr) {
        delete m_fileSystem;
    }
}

void NMSSHSftpFileListModel::load()
{
    clear();

    QList<FileItem> fileItems = m_fileSystem->loadFiles(QUrl(m_path));

    qWarning() << fileItems.size() << m_path;

    for (FileItem fileItem : fileItems) {
        //if (fileItem.isHidden()) {
        //    continue;
        //}
        QStandardItem *item = new QStandardItem();
        item->setData(fileItem.name(), FileListModel::NameRole);
        appendRow(item);
    }
}
