#ifndef __NMSSH_SFTP_FILE_SYSTEM__
#define __NMSSH_SFTP_FILE_SYSTEM__

#include "filesystem.h"
#include <NMSSH.h>

#include <QPixmap>

Q_FORWARD_DECLARE_OBJC_CLASS(NSImage);

class NMSSHSftpFileItem : public FileItem
{
public:
    NMSSHSftpFileItem(NMSFTPFile *sftpFile, QUrl parentUrl, QString user);
private:
    QPixmap fromNSImageToPixmap(NSImage *nsImage, const int width, const int height);
};

class NMSSHSftpFileSystem : public FileSystem
{
public:
    NMSSHSftpFileSystem(const QString name, const QString host, const int port, const QString user, const QString password, const QString path);
    ~NMSSHSftpFileSystem();

    // Read file list for provided URL. URL must reside under rootUrl.
    QList<FileItem> loadFiles(const QUrl url);

    // Delete file at provided URL. URL must reside under rootUrl
    void deleteFile(const QUrl url);

    // Copy file from one place to another. At least one URL must reside under rootUrl. destUrl must indicate the
    // final copied file name, not the directory containing it.
    void copyFile(const QUrl srcUrl, const QUrl destUrl);

    // Move file from one place to another. Both URLs must reside under rootUrl
    void moveFile(const QUrl srcUrl, const QUrl destUrl);

    // Create folder at specified url, which must be under file system root
    void createFolder(const QUrl url);
private:
    NMSSHSession *m_browseSession;
    NMSSHSession *m_fileOperationsSession;
};

class NMSSHSftpFileListModel : public FileListModel
{
public:
    NMSSHSftpFileListModel(QObject* parent = nullptr);
    ~NMSSHSftpFileListModel();

    void load();
};

#endif